import datetime as dt
import pandas as pd
import numpy as np
from Cleaning_functions import *
import csv
from collections import OrderedDict
import os

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)
#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________
import time; tt = time.time()

# Define datapath, filename and language:
datafolder = input("Enter directory of original data: ")
datafile = input("Enter name of original data file (excel and csv files are accepted): ")

# Check if the datafile exists:
mypath = os.path.join(datafolder, datafile)
assert os.path.exists(mypath), "File not found at " + str(mypath)

# Get the language and also check, if the data contains brasilian and other data:
language = input("Enter the language of the data to be processed. 'Spanish' and 'Portuguese' are allowed. ")
mixed_data = input('Does the file contain both Brasilian and other LATAM data? (y/n) ')
assert mixed_data in ['y', 'n'], 'Please answer y or n.'

# Check, if the data contains combos:
run_combos = input('Could the data contain combos? (y/n) ')
assert run_combos in ['y', 'n'], 'Please answer y or n.'

# Check, if the data contains packs:
run_packs = input('Could the data contain packs? (y/n) ')
assert run_packs in ['y', 'n'], 'Please answer y or n.'

# Get the file extension:
extension = datafile.split('.')[-1]

# Load the data:
assert extension in ['csv', 'xlsx', 'xls'], str(extension) + ' is not a valid file type.'
if extension == 'csv':
    df = pd.read_csv(mypath)
elif (extension in ['xlsx', 'xls']):
    df = pd.read_excel(mypath)

# Change header to lowercase:
df.columns = [col.lower() for col in df.columns]

# Remove items without a name:
df = df[~df.name.isna()]
df.name = df.name.astype(str)

## Make sure the columns are loaded as string:
for col in ['name', 'description']:
    df[col] = df[col].astype(str)

# Add flag for unchanged units:
df['unit_remains'] = 0

# Get only the items in the current language, if the data is mixed:
if mixed_data == 'y':
    assert 'country' in df.columns, 'For data from both Brasil and other LATAM country you need to provide a country.'
    if language == 'Spanish':
        df = df[df.country.str.lower()!='br']
    elif language == 'Portuguese':
        df = df[df.country.str.lower()=='br']

if language == 'Spanish':
    # Get the unit_type dictionary from csv file:
    with open('unit_dict_spanish.csv') as csv_file:
        reader = csv.reader(csv_file)
        unit_dict = OrderedDict(reader)
    with open('unit_dict_spanish_cleanup.csv') as csv_file:
        reader = csv.reader(csv_file)
        unit_dict_cleanup = OrderedDict(reader)
    # Get the types for c/u processing from csv file:
    with open('types.csv') as f:
        types = f.read().splitlines()
elif language == 'Portuguese':
    # Get dictionaries for Portuguese data:
    with open('unit_dict_portuguese.csv') as csv_file:
        reader = csv.reader(csv_file)
        unit_dict = OrderedDict(reader)
    with open('unit_dict_portuguese_cleanup.csv') as csv_file:
        reader = csv.reader(csv_file)
        unit_dict_cleanup = OrderedDict(reader)

# Check, if all the necessary columns are in the data, and warn if not:
if (run_combos == 'n') and (run_packs == 'n'):
    mandatory_columns = ['id', 'quantity', 'unit_type', 'name', 'description', 'presentation', 'type']
else:
    mandatory_columns = ['id', 'quantity', 'unit_type', 'name', 'description', 'presentation', 'discount_description', 'discount_type', 'type']
missing_columns = [item for item in mandatory_columns if item not in df.columns]
if len(missing_columns) > 0:
    print('WARNING MISSING DATA!')
    for i in range(len(missing_columns)):
        print(missing_columns[i] + ' is missing.')
        
if 'type' not in df.columns:
    df['type'] = 'type'
if 'country' not in df.columns:
    df['country'] = ''
        
# Make sure quantity is a number:
if 'quantity' in df.columns:
    df['quantity'] = df['quantity'].astype(float)
#_______________________________________________________________________________________________________________________
# Clean the unit type
#_______________________________________________________________________________________________________________________
# In this step, all unit types are normalized, e.g. 'Unidade', 'unid.0', ... are matched to 'Und', as specified by Rappi.
# Delete numbers
if 'unit_type' in df.columns:
    if sum(df.unit_type.isna()) < len(df):
        df.loc[~df.unit_type.isna(), 'unit_type'] = df.loc[~df.unit_type.isna(), 'unit_type'].str.replace('\d*.?' + '\d+', '', regex=True).str.strip()
        # Rename the unit_types to the standardized one:
        # df.unit_type = df.unit_type.apply(lambda x: x.capitalize() if (type(x) == str) and (x not in ['ml','g','mg','cm','cc','m','yd']) else x)
        # Plural:
        plural_dict = {key + 's':value for key, value in unit_dict_cleanup.items()}
        df.unit_type = df.unit_type.str.lower().replace(plural_dict)
        del(plural_dict)
        # With dots:
        comma_dict = {key + '.':value for key, value in unit_dict_cleanup.items()}
        df.unit_type = df.unit_type.str.lower().replace(comma_dict)
        del(comma_dict)
        # Singular:
        df.unit_type = df.unit_type.str.lower().replace(unit_dict_cleanup)
else:
    df['unit_type'] = ''
# Keep a copy for testing:
df_orig = df.copy()

print('Unit type mapping DONE.')
#_______________________________________________________________________________________________________________________
# Clean name general
#_______________________________________________________________________________________________________________________
# Find names with a ',' as a decimal separator and replace it by a '.':
regex = '(?<=\d),(?=\d)' # Number comma number, i.e. 1,5
df.name = df.name.apply(lambda x: re.sub(regex, '.', x)) # Now is 1.5

# Find names that have line formatting in the name and remove it:
mystrs = ['\t', '\r', '\n']
for ss in mystrs:
    for col in ['name', 'description']:
        df[col] = df[col].str.replace(ss, '')

# Split unit and quantity, remove multiple spaces:
# Define the regular expressions necessary to separate unit and quantity with a space:
myregex = list()
for key, value in unit_dict.items():
    myregex.append(r'{}\{}\{}\{}'.format('(?i)(x?', 'd*', '.?', 'd+' + key + 's? )'))
    # myregex.append(r'{}\{}'.format('(?i)(x?', 'd+' + key + 's? )'))

# Split unit and quantity:
df.name = df.name.apply(split_unit_quantity, args=(myregex, ))

# Tidy up double spaces:
df.name = df.name.apply(remove_multiple_spaces)

# Remove trailing and leading whitespaces from type:
if 'type' in df.columns:
    if sum(df.type.isna()) < len(df):
        df.type = df.type.str.strip()

print('General cleaning DONE.')
# Capitalize words in the name, except for connectors:
#df.name = df.name.apply(capitalize_name)

#_______________________________________________________________________________________________________________________
# Combos and Rappicombos:
#_______________________________________________________________________________________________________________________
if run_combos == 'y':
    # Identify combos and rappicombos.
    # First deal with items where the name contains Rappicombo or Combo:
    # Rappicombos:
    df.loc[df.name.str.lower().str.contains('rappicombo', na=False), 'tmp'] = 1
    df.loc[df.name.str.lower().str.contains('rappi combo', na=False), 'tmp'] = 1
    df.loc[df.name.str.lower().str.contains('rapicombo', na=False), 'tmp'] = 1
    # Change unit_type and quantity:
    df.loc[df.tmp==1, 'unit_type'] = 'Und'
    df.loc[df.tmp==1, 'quantity'] = 1
    # Change type to 'Rappicombo':
    df.loc[df.tmp==1, 'type'] = 'Rappicombo'
    # Change name for misspelled cases:
    df.loc[df.tmp==1, 'name']= df.loc[df.tmp==1, 'name'].str.replace('Rapicombo', 'Rappicombo')
    df.loc[df.tmp==1, 'name'] = df.loc[df.tmp==1, 'name'].str.replace('Rappi combo', 'Rappicombo')
    # Flag that the name should not be changed:
    df.loc[df.tmp==1, 'unit_remains'] = 1

    # Combos: Name contains a '+':
    df.loc[(df.unit_remains==0) & (df.name.str.contains(' ?\+ ?', regex=True)), 'tmp'] = 2
    # Name starts with combo:
    mask = (df.tmp==2)
    df.loc[mask & (df[mask].name.str.contains('(?i)(^combo ?)', regex=True)), 'tmp'] = 3
    # Type contains 'combo':
    df.loc[mask & (df[mask].type.str.contains('(?i)(combo)', regex=True)), 'tmp'] = 3
    # Change type to 'Combo':
    df.loc[df.tmp==3, 'type'] = 'Combo'
    # Change unit_type and quantity:
    df.loc[df.tmp==3, 'unit_type'] = 'Und'
    df.loc[df.tmp==3, 'quantity'] = 1
    # Flag that the name should not be changed:
    df.loc[df.tmp==3, 'unit_remains'] = 1
    # Drop helper column:
    df.drop('tmp', axis=1, inplace=True)

    # Now deal with the items where the type contains Rappicombo
    # Rappicombos:
    df.loc[df.type.str.lower().str.contains('rappicombo', na=False), 'tmp'] = 1
    df.loc[df.type.str.lower().str.contains('rappi combo', na=False), 'tmp'] = 1
    df.loc[df.type.str.lower().str.contains('rapicombo', na=False), 'tmp'] = 1
    # Change unit_type and quantity:
    mask=(df.tmp==1)
    df.loc[mask & (df[mask].unit_remains==0), 'unit_type'] = 'Und'
    df.loc[mask & (df[mask].unit_remains==0), 'quantity'] = 1
    # Change type to 'Rappicombo':
    df.loc[mask & (df[mask].unit_remains==0), 'type'] = 'Rappicombo'
    # Flag that the name should not be changed in the future.
    df.loc[mask & (df[mask].unit_remains==0), 'unit_remains'] = 1

    # Combos: name contains 'combo quantity or starts with a quantity'; these are actually packs!
    df.loc[(df.unit_remains==0) & (df.tmp!=1) & (df.name.str.lower().str.contains('(combo \d+ (?!up))', regex=True)), 'tmp'] = 2
    df.loc[(df.unit_remains==0) & (df.tmp!=1) & (df.name.str.lower().str.contains('(?i)(^\d+x? (?!up))', regex=True)), 'tmp'] = 2
    # Change type to 'Pack':
    df.loc[df.tmp==2, 'type'] = 'Pack'
    # Remove 'Combo' from name:
    df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].str.replace('Combo', '')
    # Now the rest of the processing of these items is the same as the processing for packs, thus, will be done later all together.

    # Combos that are actually promos, start with 'combo' but then are followed by quantity x quantity (deal).
    regex = '(?i)^combo ?\d+ ?x ?\d+ '
    df.loc[(~df.tmp.isin([1, 2])) & (df.unit_remains==0) & (df.name.str.lower().str.contains(regex, regex=True)), 'tmp'] = 4
    # Change the name (remove combo):
    df.loc[df.tmp==4, 'name'] = df.loc[df.tmp==4, 'name'].str.replace('(?i)^combo', '').str.strip()
    # Now the items will be recognized as promotions and processed correctly.

    # Combos that are actually promos, start with quantity x quantity (deal).
    regex = '(?i)^\d+ ?x ?\d+ '
    df.loc[(~df.tmp.isin([1, 2])) & (df.unit_remains==0) & (df.name.str.lower().str.contains(regex, regex=True)), 'tmp'] = 5
    # Now the items will be recognized as promotions and processed correctly.

    # Combos: Type contains 'Combo' and no other rule applies.
    df.loc[(~df.tmp.isin([1, 2, 4, 5])) & (df.unit_remains==0) & (df.type.str.lower().str.contains('combo')), 'tmp'] = 3
    # Change type to 'Combo':
    df.loc[df.tmp==3, 'type'] = 'Combo'
    # Change unit_type and quantity:
    df.loc[df.tmp==3, 'unit_type'] = 'Und'
    df.loc[df.tmp==3, 'quantity'] = 1
    # Flag that the name should not be changed:
    df.loc[df.tmp==3, 'unit_remains'] = 1
    # Drop helper column:
    df.drop('tmp', axis=1, inplace=True)

    print('Combos and Rappicombos DONE.')
#_______________________________________________________________________________________________________________________
# Products where quantity is given per unit
#_______________________________________________________________________________________________________________________
# So far, the Portuguese database does not seem to have this type of item. Only execute it for the Spanish database:
if language == 'Spanish':
    # Flag anything that has 'c/u' in the title:
    df.loc[(df.unit_remains==0) & (df.name.str.lower().str.contains('c/u')), 'tmp'] = 1

    # Case 1: name has X quantity X quantity unit_type C/U:
    myregex = list()
    for key in unit_dict.keys():
        myregex.append(r'{}\{}\{}'.format('(?i)(x ?', 'd+ ?x ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    mask = (df.tmp==1)
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 2
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==2), 'presentation'] = df.loc[(df.tmp==2), 'name'].apply(presentation_cu, args=(regex,))
    df.loc[(df.tmp==2), 'quantity'] = df.loc[(df.tmp==2), 'name'].apply(quantity_cu, args=(regex, ))
    df.loc[(df.tmp==2), 'unit_type'] = df.loc[(df.tmp==2), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==2), 'discount_description'] = df.loc[packmask & (df.tmp==2), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==2), 'name'] = df.loc[(df.tmp==2), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==2), 'name'] = df.loc[(df.tmp==2), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==2), 'presentation'] = df.loc[(df.tmp==2), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==2), 'unit_remains'] = 1

    # Case 2: name has X quantity TYPE X quantity unit_type C/U
    # Define possible types:
    myregex = list()
    for key in unit_dict.keys():
        for mytype in types:
            myregex.append(r'{}\{}\{}'.format('(?i)(x ?', 'd+ ?' + mytype + 's? ?x ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 3
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==3), 'presentation'] = df.loc[(df.tmp==3), 'name'].apply(presentation_cu, args=(regex, 2, types))
    df.loc[(df.tmp==3), 'quantity'] = df.loc[(df.tmp==3), 'name'].apply(quantity_cu, args=(regex, ))
    df.loc[(df.tmp==3), 'unit_type'] = df.loc[(df.tmp==3), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==3), 'discount_description'] = df.loc[packmask & (df.tmp==3), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==3), 'name'] = df.loc[(df.tmp==3), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==3), 'name'] = df.loc[(df.tmp==3), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==3), 'presentation'] = df.loc[(df.tmp==3), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==3), 'unit_remains'] = 1

    # Case 3: name has con quantity type con quantity unit_type c/u
    myregex = list()
    for key in unit_dict.keys():
        for mytype in types:
            myregex.append(r'{}\{}\{}'.format('(?i)( con ?', 'd+ ?' + mytype + 's?.? ?con ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 4
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==4), 'presentation'] = df.loc[(df.tmp==4), 'name'].apply(presentation_cu, args=(regex, 88, types))
    df.loc[(df.tmp==4), 'quantity'] = df.loc[(df.tmp==4), 'name'].apply(quantity_cu, args=(regex, 1)) # Multiply units #TODO: Check with Catalina
    df.loc[(df.tmp==4), 'unit_type'] = df.loc[(df.tmp==4), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==4), 'discount_description'] = df.loc[packmask & (df.tmp==4), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==4), 'name'] = df.loc[(df.tmp==4), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==4), 'name'] = df.loc[(df.tmp==4), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==4), 'presentation'] = df.loc[(df.tmp==4), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==4), 'unit_remains'] = 1

    # Case 4: name has con quantity unit_type con quantity unit_type c/u
    myregex = list()
    for key1 in unit_dict.keys():
        for key2 in unit_dict.keys():
            myregex.append(r'{}\{}\{}'.format('(?i)( con ?', 'd+ ?' + key1 + 's?.? ?con ?', 'd+ ?' + key2 + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 5
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==5), 'presentation'] = df.loc[(df.tmp==5), 'name'].apply(presentation_cu, args=(regex, 88))
    df.loc[(df.tmp==5), 'quantity'] = df.loc[(df.tmp==5), 'name'].apply(quantity_cu, args=(regex, 2)) # Don't multiply, take last one.
    df.loc[(df.tmp==5), 'unit_type'] = df.loc[(df.tmp==5), 'name'].apply(unit_cu, args=(regex, unit_dict, 2))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==5), 'discount_description'] = df.loc[packmask & (df.tmp==5), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==5), 'name'] = df.loc[(df.tmp==5), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==5), 'name'] = df.loc[(df.tmp==5), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==5), 'presentation'] = df.loc[(df.tmp==5), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==5), 'unit_remains'] = 1

    # Case 5: name has con quantity unit_type con quantity unit_type c/u
    myregex = list()
    for key1 in unit_dict.keys():
        for key2 in unit_dict.keys():
            myregex.append(r'{}\{}\{}'.format('(?i)( ', 'd+ ?' + key1 + 's?.? ?con ?', 'd+ ?' + key2 + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 6
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==6), 'presentation'] = df.loc[(df.tmp==6), 'name'].apply(presentation_cu, args=(regex, 88))
    df.loc[(df.tmp==6), 'quantity'] = df.loc[(df.tmp==6), 'name'].apply(quantity_cu, args=(regex, 2)) # Don't multiply, take last one.
    df.loc[(df.tmp==6), 'unit_type'] = df.loc[(df.tmp==6), 'name'].apply(unit_cu, args=(regex, unit_dict, 2))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==6), 'discount_description'] = df.loc[packmask & (df.tmp==6), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==6), 'name'] = df.loc[(df.tmp==6), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==6), 'name'] = df.loc[(df.tmp==6), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==6), 'presentation'] = df.loc[(df.tmp==6), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==6), 'unit_remains'] = 1

    # Case 6: name has quantity x quantity unit_type c/u
    myregex = list()
    for key in unit_dict.keys():
        myregex.append(r'{}\{}\{}'.format('(?i)( ?', 'd+ ?x ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 7
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==7), 'presentation'] = df.loc[(df.tmp==7), 'name'].apply(presentation_cu, args=(regex,))
    df.loc[(df.tmp==7), 'quantity'] = df.loc[(df.tmp==7), 'name'].apply(quantity_cu, args=(regex, 2))
    df.loc[(df.tmp==7), 'unit_type'] = df.loc[(df.tmp==7), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==7), 'discount_description'] = df.loc[packmask & (df.tmp==7), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==7), 'name'] = df.loc[(df.tmp==7), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==7), 'name'] = df.loc[(df.tmp==7), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==7), 'presentation'] = df.loc[(df.tmp==7), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==7), 'unit_remains'] = 1

    # Case 7: quantity type con quantity unit_type c/u:
    myregex = list()
    for key in unit_dict.keys():
        for mytype in types:
            myregex.append(r'{}\{}\{}'.format('(?i)( ', 'd+ ?' + mytype + 's?.? ?con ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 8
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==8), 'presentation'] = df.loc[(df.tmp==8), 'name'].apply(presentation_cu, args=(regex, 88, types))
    df.loc[(df.tmp==8), 'quantity'] = df.loc[(df.tmp==8), 'name'].apply(quantity_cu, args=(regex, 1)) # Multiply units #TODO: Check with Catalina
    df.loc[(df.tmp==8), 'unit_type'] = df.loc[(df.tmp==8), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==8), 'discount_description'] = df.loc[packmask & (df.tmp==8), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==8), 'name'] = df.loc[(df.tmp==8), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==8), 'name'] = df.loc[(df.tmp==8), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==8), 'presentation'] = df.loc[(df.tmp==8), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==8), 'unit_remains'] = 1

    # Case 8: name has quantity TYPE  quantity unit_type C/U
    myregex = list()
    for key in unit_dict.keys():
        for mytype in types:
            myregex.append(r'{}\{}\{}'.format('(?i)( ?', 'd+ ?' + mytype + 's?.? ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 9
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==9), 'presentation'] = df.loc[(df.tmp==9), 'name'].apply(presentation_cu, args=(regex, 3, types))
    df.loc[(df.tmp==9), 'quantity'] = df.loc[(df.tmp==9), 'name'].apply(quantity_cu, args=(regex, 2)) # Don't multiply units
    df.loc[(df.tmp==9), 'unit_type'] = df.loc[(df.tmp==9), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==9), 'discount_description'] = df.loc[packmask & (df.tmp==9), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==9), 'name'] = df.loc[(df.tmp==9), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==9), 'name'] = df.loc[(df.tmp==9), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==9), 'presentation'] = df.loc[(df.tmp==9), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==9), 'unit_remains'] = 1

    # Case 9: quantity TYPE de quantity unit_type c/u
    myregex = list()
    for key in unit_dict.keys():
        for mytype in types:
            myregex.append(r'{}\{}\{}'.format('(?i)(', 'd+ ?' + mytype + 's?.? ?de ?', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 10
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==10), 'presentation'] = df.loc[(df.tmp==10), 'name'].apply(presentation_cu, args=(regex, 3, types))
    df.loc[(df.tmp==10), 'quantity'] = df.loc[(df.tmp==10), 'name'].apply(quantity_cu, args=(regex, )) # Multiply units
    df.loc[(df.tmp==10), 'unit_type'] = df.loc[(df.tmp==10), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==10), 'discount_description'] = df.loc[packmask & (df.tmp==10), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==10), 'name'] = df.loc[(df.tmp==10), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==10), 'name'] = df.loc[(df.tmp==10), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==10), 'presentation'] = df.loc[(df.tmp==10), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==10), 'unit_remains'] = 1

    # Case 10: de/x quantity unit_type c/u
    myregex = list()
    for key in unit_dict.keys():
        myregex.append(r'{}\{}'.format('(?i)(de ?', 'd+ ?' + key + 's? ?c/u)'))
        myregex.append(r'{}\{}'.format('(?i)(x ?', 'd+ ?' + key + 's? ?c/u)'))
        myregex.append(r'{}\{}'.format('(?i)( ', 'd+ ?' + key + 's? ?c/u)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 11
    # Add quantity and unit_type:
    df.loc[(df.tmp==11), 'quantity'] = df.loc[(df.tmp==11), 'name'].apply(quantity_cu, args=(regex, 3)) # Just one unit
    df.loc[(df.tmp==11), 'unit_type'] = df.loc[(df.tmp==11), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Clean the name:
    df.loc[(df.tmp==11), 'name'] = df.loc[(df.tmp==11), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==11), 'name'] = df.loc[(df.tmp==11), 'name'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==11), 'unit_remains'] = 1

    # Case 11: quantity unit_type c/u quantity type
    myregex = list()
    for key in unit_dict.keys():
        for mytype in types:
            myregex.append(r'{}\{}\{}'.format('(?i)(', 'd+ ?' + key + 's?.? ?c/u.? ?', 'd+ ?' + mytype + 's? ?)'))
    regex = '|'.join(myregex)
    # Flag them:
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 12
    # Add presentation, quantity and unit_type:
    df.loc[(df.tmp==12), 'presentation'] = df.loc[(df.tmp==12), 'name'].apply(presentation_cu, args=(regex, 33, types))
    df.loc[(df.tmp==12), 'quantity'] = df.loc[(df.tmp==12), 'name'].apply(quantity_cu, args=(regex, )) # Multiply units
    df.loc[(df.tmp==12), 'unit_type'] = df.loc[(df.tmp==12), 'name'].apply(unit_cu, args=(regex, unit_dict))
    # Add the discount_description for packs:
    packmask = (df.type=='Pack')
    df.loc[packmask & (df.tmp==12), 'discount_description'] = df.loc[packmask & (df.tmp==12), 'name'].apply(quantity_cu, args=(regex, 3)) # return first number
    # Clean the name:
    df.loc[(df.tmp==12), 'name'] = df.loc[(df.tmp==12), 'name'].apply(remove_unit, args=(regex,))
    # Remove whitespaces:
    df.loc[(df.tmp==12), 'name'] = df.loc[(df.tmp==12), 'name'].apply(remove_multiple_spaces)
    df.loc[(df.tmp==12), 'presentation'] = df.loc[(df.tmp==12), 'presentation'].apply(remove_multiple_spaces)
    # Flag to be unchanged in the future:
    df.loc[(df.tmp==12), 'unit_remains'] = 1

    # Case 12: name has C/U and it doesn't fall into one of the cases above
    df.loc[(df.tmp==1), 'unit_remains'] = 1 # Always leave name unchanged.

    # Drop the helper columns:
    df.drop('tmp', axis=1, inplace=True)

    print('Products with quantities by unit DONE.')
#_______________________________________________________________________________________________________________________
# Items that are square or cubic, i.e. contain 'xXycm' or something similar in the name.
#_______________________________________________________________________________________________________________________
# Flag items that contain a unit twice to reduce the search space for the regex:
values = ['cm', 'mm', 'm']
for value in values:
    df['mycount'] =df.name.str.count(value)
    df.loc[df.mycount > 1, 'tmp'] = 99
df.drop('mycount', axis=1, inplace=True)

myregex = list() #['(?i)( \d+ ?x ?\d+ )']
for key1, value1 in unit_dict.items():
    if value1 in values:
        for key2, value2 in unit_dict.items():
            if value2 in values:
                # myregex.append('(?i)( \d+ ?' + key1 + '? ?x ?\d+ ?' + key2 + 's?)')
                # myregex.append('(?i)( \d+ ?' + key1 + '? ?x ?\d*.\d+ ?' + key2 + 's?)')
                # myregex.append('(?i)( \d*.\d+ ?' + key1 + '? ?x ?\d+ ?' + key2 + 's?)')
                myregex.append('(?i)( \d*\.?\d+ ?' + key1 + '? ?x ?\d*\.?\d+ ?' + key2 + 's?)')
regex = '|'.join(myregex)
# Flag the ones that do:
mask = ((df.type!='Pack')&(df.tmp==99))
df.loc[mask & df[mask].name.str.contains(regex, regex=True) & (df[mask].unit_type.str.lower().isin([key for key, value in unit_dict.items() if value=='Und'])), 'tmp'] = 1
# Remove units (only of type unit) if they exist:
# Flag to not change the items afterwards:
df.loc[df.tmp==1, 'unit_remains'] = 1
# Remove all units from name:
myregex = list()
for key, value in unit_dict.items():
    if value=='Und':
        myregex.append(r'{}\{}\{}\{}'.format('(?i)(x? ?', 'd*', '.?', 'd+ ?' + key + 's?)'))
        # myregex.append(r'{}\{}'.format('(?i)(x? ?', 'd+ ?' + key + 's?)'))
regex = '|'.join(myregex)
# Flag the ones that have an extra unit:
mask = (df.tmp==1)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 2
# Put units in quantity:
df.loc[df.tmp==2, 'quantity'] = df.loc[df.tmp==2, 'name'].apply(set_quantity_square, args=(regex,))
# Set unit_type to 'Und':
df.loc[df.tmp==2, 'unit_type'] = 'Und'
# Remove unit:
df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_unit, args=(regex,))
# Remove double spaces:
df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_multiple_spaces)
# Drop helper column:
df.drop('tmp', axis=1, inplace=True)
print('Unit x unit DONE.')
#_______________________________________________________________________________________________________________________
# Identify and clean promos
#_______________________________________________________________________________________________________________________

# Promos x% off x unit:

myregex = ['(?i)(\d+% ?off ?en ?\d+ ?.?u)', '(?i)(\d+% ?off ?en ?segunda ?.?u)', '(?i)(\d+% ?off ?segunda ?.?u)']
combined = '|'.join(myregex)
# Find the promos:
df.loc[df.name.str.contains(combined, regex=True) & (df.unit_remains==0), 'tmp'] = 1
# Change the discount_type and type:
discount_type = 'percentage'
df.loc[df.tmp==1, 'discount_type'] = discount_type
df.loc[df.tmp==1, 'type'] = 'Promo'
# Change discount description and name for names that contain number and unit:
regex = '(?i)(\d+% ?off ?en ?\d+ ?.?u)'
mask = (df.tmp==1)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(get_discount_description, args=(regex, discount_type))
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(remove_unit, args=(regex, ))
# Change discount description and name for names with promo type x% off en segunda unidad:
regex = '(?i)(\d+% ?off ?en ?segunda ?.?u)'
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(get_discount_description, args=(regex, discount_type))
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = '2|' + df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'].astype(str)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(remove_unit, args=(regex, ))
# Change discount description for names with promo type x% off segunda unidad:
regex = '(?i)(\d+% ?off ?segunda ?.?u)'
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(get_discount_description, args=(regex, discount_type))
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = '2|' + df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'].astype(str)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(remove_unit, args=(regex, ))

# Promos x% off:
regex = '(?i)(\d+% ?off(?! ?en))'
mask = (df.tmp!=1)
# Find the promos:
df.loc[mask & df[mask].name.str.contains(regex, regex=True) & (df[mask].unit_remains==0), 'tmp'] = 2
# Change the discount_type:
discount_type = 'markdown'
df.loc[df.tmp==2, 'discount_type'] = discount_type
df.loc[df.tmp==2, 'type'] = 'Promo'
# Change the discount_description and name:
df.loc[df.tmp==2, 'discount_description'] =  df.loc[df.tmp==2, 'name'].apply(get_discount_description, args=(regex, discount_type))
df.loc[df.tmp==2, 'name'] =  df.loc[df.tmp==2, 'name'].apply(remove_unit, args=(regex, ))

# Promos buy x get x free:

myregex = ['(?i)(l?leva ?\d+ ?paga)', '(?i)(l?leve ?\d+ ?pague)', '(?i)(pague ?\d+ ?l?leve)', '(?i)(paga ?\d+ ?l?leva)', '(?i)(buy ?\d+ ?get one free)', '(?i)(buy ?\d+ ?get ?\d+ ?free)']
regex = '|'.join(myregex)
# Find the promos:
mask = (~df.tmp.isin([1, 2]))
df.loc[mask & df[mask].name.str.contains(regex, regex=True) & (df[mask].unit_remains==0), 'tmp'] = 3
# Change the discount_type:
discount_type = 'deal'
df.loc[df.tmp==3, 'discount_type'] = discount_type
df.loc[df.tmp==3, 'type'] = 'Promo'
# Change discount description for names that contain 'buy x get one free':
regex = '(?i)(buy ?\d+ ?get one free)'
mask = (df.tmp==3)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 31
df.loc[df.tmp==31, 'discount_description'] = df.loc[df.tmp==31, 'name'].apply(get_discount_description, args=(regex,discount_type)) + '|' + str(df.loc[df.tmp==31, 'name'].apply(get_discount_description, args=(regex,discount_type)))
df.loc[df.tmp==31, 'name'] = df.loc[df.tmp==31, 'name'].apply(remove_unit, args=(regex,))
df.loc[df.tmp==31, 'presentation'] = df.loc[df.tmp==31, 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[df.tmp==31, 'presentation'] = df.loc[df.tmp==31, 'presentation'] + ' ' + df.loc[df.tmp==31, 'unit_type']
mask_31 = (df.tmp==31)
df.loc[mask_31 & (df[mask_31].quantity.isna()), 'quantity'] = df.loc[mask_31 & (df[mask_31].quantity.isna()), 'discount_description'].apply(lambda x: x.split('|')[-1])
# Change discount description for names that are of the structure buy x get quantity unit_type y:
myregex = list()
for key in unit_dict.keys():
    myregex.append(r'{}\{}\{}\{}'.format('(?i)(pague ?', 'd+ ?l?leve ?', 'd+ ?' + key + 's? ?x? ?', 'd+)($|\s)'))
    myregex.append(r'{}\{}\{}\{}'.format('(?i)(paga ?', 'd+ ?l?leva ?', 'd+ ?' + key + 's? ?x? ?', 'd+)($|\s)'))
regex = '|'.join(myregex)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 32
df.loc[df.tmp==32, 'discount_description'] = df.loc[df.tmp==32, 'name'].apply(get_discount_description, args=(regex, 'Deal_inv_quantity'))
df.loc[df.tmp==32, 'quantity'] = df.loc[df.tmp==32, 'name'].apply(get_quantity_discount, args=(regex, ))
df.loc[df.tmp==32, 'unit_type'] = df.loc[df.tmp==32, 'name'].apply(get_unit_type_discount, args=(unit_dict, regex))
df.loc[df.tmp==32, 'name'] = df.loc[df.tmp==32, 'name'].apply(remove_unit, args=(regex, ))
df.loc[df.tmp==32, 'presentation'] = df.loc[df.tmp==32, 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[df.tmp==32, 'presentation'] = df.loc[df.tmp==32, 'presentation'] + ' ' + df.loc[df.tmp==32, 'unit_type']
mask_32 = (df.tmp==32)
df.loc[mask_32 & (df[mask_32].quantity.isna()), 'quantity'] = df.loc[mask_32 & (df[mask_32].quantity.isna()), 'discount_description'].apply(lambda x: x.split('|')[-1])
# Change discount description for names that are of the structure buy x get y unit_type :
myregex = list()
for key in unit_dict.keys():
    myregex.append(r'{}\{}\{}'.format('(?i)(pague ?', 'd+ ?l?leve ?', 'd+ ?' + key + 's?)($|\s)'))
    myregex.append(r'{}\{}\{}'.format('(?i)(paga ?', 'd+ ?l?leva ?', 'd+ ?' + key + 's?)($|\s)'))
regex = '|'.join(myregex)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 33
df.loc[df.tmp==33, 'discount_description'] = df.loc[df.tmp==33, 'name'].apply(get_discount_description, args=(regex, 'Deal_inv'))
df.loc[df.tmp==33, 'quantity'] = df.loc[df.tmp==33, 'name'].apply(get_quantity_discount, args=(regex, ))
df.loc[df.tmp==33, 'unit_type'] = df.loc[df.tmp==33, 'name'].apply(get_unit_type_discount, args=(unit_dict, regex))
df.loc[df.tmp==33, 'name'] = df.loc[df.tmp==33, 'name'].apply(remove_unit, args=(regex, ))
df.loc[df.tmp==33, 'presentation'] = df.loc[df.tmp==33, 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[df.tmp==33, 'presentation'] = df.loc[df.tmp==33, 'presentation'] + ' ' + df.loc[df.tmp==33, 'unit_type']
mask_33 = (df.tmp==33)
df.loc[mask_33 & (df[mask_33].quantity.isna()), 'quantity'] = df.loc[mask_33 & (df[mask_33].quantity.isna()), 'discount_description'].apply(lambda x: x.split('|')[-1])
# Change discount description for names that are of the structure buy x get y unit_type :
myregex = list()
for key in unit_dict.keys():
    myregex.append(r'{}\{}\{}'.format('(?i)(l?leve ?', 'd+ pague ?', 'd+ ?' + key + 's?)($|\s)'))
    myregex.append(r'{}\{}\{}'.format('(?i)(l?leva ?', 'd+ paga ?', 'd+ ?' + key + 's?)($|\s)'))
regex = '|'.join(myregex)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 34
df.loc[df.tmp==34, 'discount_description'] = df.loc[df.tmp==34, 'name'].apply(get_discount_description, args=(regex, 'deal'))
df.loc[df.tmp==34, 'quantity'] = df.loc[df.tmp==34, 'name'].apply(get_quantity_discount, args=(regex, ))
df.loc[df.tmp==34, 'unit_type'] = df.loc[df.tmp==34, 'name'].apply(get_unit_type_discount, args=(unit_dict, regex))
df.loc[df.tmp==34, 'name'] = df.loc[df.tmp==34, 'name'].apply(remove_unit, args=(regex, ))
df.loc[df.tmp==34, 'presentation'] = df.loc[df.tmp==34, 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[df.tmp==34, 'presentation'] = df.loc[df.tmp==34, 'presentation'] + ' ' + df.loc[df.tmp==34, 'unit_type']
mask_34 = (df.tmp==34)
df.loc[mask_34 & (df[mask_34].quantity.isna()), 'quantity'] = df.loc[mask_34 & (df[mask_34].quantity.isna()), 'discount_description'].apply(lambda x: x.split('|')[-1])
#
# Change discount description for names that contain 'buy x get y free':
myregex = ['(?i)(l?leve ?\d+ ?pague ?\d+)', '(?i)(l?leva ?\d+ ?pagua ?\d+)']
regex = '|'.join(myregex)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 35
df.loc[df.tmp==35, 'discount_description'] = df.loc[df.tmp==35, 'name'].apply(get_discount_description, args=(regex, discount_type))
df.loc[df.tmp==35, 'name'] = df.loc[df.tmp==35, 'name'].apply(remove_unit, args=(regex, ))
df.loc[df.tmp==35, 'presentation'] = df.loc[df.tmp==35, 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[df.tmp==35, 'presentation'] = df.loc[df.tmp==35, 'presentation'] + ' ' + df.loc[df.tmp==35, 'unit_type']
mask_35 = (df.tmp==35)
df.loc[mask_35 & (df[mask_35].quantity.isna()), 'quantity'] = df.loc[mask_35 & (df[mask_35].quantity.isna()), 'discount_description'].apply(lambda x: x.split('|')[-1])

# Change discount description for names that contain 'buy x get y free':
myregex = ['(?i)(pague ?\d+ ?l?leve ?\d+)', '(?i)(paga ?\d+ ?l?leva ?\d+)']
regex = '|'.join(myregex)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 36
df.loc[df.tmp==36, 'discount_description'] = df.loc[df.tmp==36, 'name'].apply(get_discount_description, args=(regex, 'Deal_inv'))
df.loc[df.tmp==36, 'name'] = df.loc[df.tmp==36, 'name'].apply(remove_unit, args=(regex, ))
df.loc[df.tmp==36, 'presentation'] = df.loc[df.tmp==36, 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[df.tmp==36, 'presentation'] = df.loc[df.tmp==36, 'presentation'] + ' ' + df.loc[df.tmp==36, 'unit_type']
mask_36 = (df.tmp==36)
df.loc[mask_36 & (df[mask_36].quantity.isna()), 'quantity'] = df.loc[mask_36 & (df[mask_36].quantity.isna()), 'discount_description'].apply(lambda x: x.split('|')[-1])

# Promos x x y:

regex = '(?i)^\d+ ?x ?\d+ '
# Find the promos:
mask = (~df.tmp.isin([1, 2, 3]))
df.loc[mask & df[mask].name.str.contains(regex, regex=True) & (df.unit_remains==0), 'tmp'] = 4
# Check if this is a real promo, i.e. if x > y:
df.loc[df.tmp==4, 'tmp'] = df.loc[df.tmp==4, 'name'].apply(check_promos, args = (regex, 4))
# Change the discount_type:
discount_type = 'deal'
df.loc[df.tmp==4, 'discount_type'] = discount_type
df.loc[df.tmp==4, 'type'] = 'Promo'
# Change discount description for items that start with 'x x y'
mask = (df.tmp==4)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(get_discount_description, args=(regex,discount_type))
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'presentation'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'discount_description'].apply(lambda x: x.split('|')[-1])
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'presentation'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'presentation'] + ' ' + df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'unit_type']
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'] = df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'name'].apply(remove_unit, args=(regex,))

# Strip the additional white spaces:
df.loc[df.tmp.isin([1, 2, 3, 4]), 'name'] = df.loc[df.tmp.isin([1, 2, 3, 4]), 'name'].apply(remove_multiple_spaces)
# Drop the helper column:
df.drop('tmp', axis=1, inplace=True)

print('Promos DONE.')
#_______________________________________________________________________________________________________________________
# Pharma products
#_______________________________________________________________________________________________________________________
# Define the regular expressions necessary to separate unit and quantity with a space:
pharma_units = None
myregex = list()
for key, value in unit_dict.items():
    pharma_units = ['mL', 'Pastilla(s)', 'Pastilha(s)', 'Comprimido(s)', 'Ampolla(s)', 'Ampolleta(s)', 'Cápsula(s)', 'Gragea(s)', 'Drágea(s)']
    if value in pharma_units:
        myregex.append(r'{}\{}\{}\{}'.format('(?i)((?<![\/,\d+])', 'd+', '.', 'd+ ?' + key + 's?(?!\/))($|\s)'))
        myregex.append(r'{}\{}'.format('(?i)((?<![\/,\d+])', 'd+ ?' + key + 's?(?!\/))($|\s)'))

# Split unit and quantity:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(split_unit_quantity, args=(myregex,))

# Push the unit and quantity to the end of the name:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(unit_to_end, args=(myregex, unit_dict))

# Tidy up double spaces:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(remove_multiple_spaces)

# Split the name at spaces and commata:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(split_name, args=(unit_dict,))

# Extract the quantity and put in new field:
# Flag the ones for which this should be done:
df.loc[df.unit_remains==0, 'tmp'] = df.loc[df.unit_remains==0, 'name'].apply(lambda x: 1 if (len(x) > 1) and (x[-1].lower() in unit_dict.keys()) and (unit_dict[x[-1].lower()] in pharma_units) and (x[-2].replace('.','',1).isdigit()) and (len(x)>1) else 0)
df.loc[(df.unit_remains==0)&(df.tmp!=1), 'tmp'] = df.loc[(df.unit_remains==0)&(df.tmp!=1), 'name'].apply(lambda x: 2 if (len(x) > 1) and (x[-1].lower()[:-1] in unit_dict.keys()) and (unit_dict[x[-1].lower()[:-1]] in pharma_units) and (x[-2].replace('.','',1).isdigit()) and (len(x)>1) else 0)
# Now make the change:
df.loc[df.tmp==1, 'quantity'] = df.loc[df.tmp==1, 'name'].apply(lambda x: x[-2])
df.loc[df.tmp==1, 'unit_type'] = df.loc[df.tmp==1, 'name'].apply(lambda x: unit_dict[x[-1].lower()])
df.loc[df.tmp==2, 'unit_type'] = df.loc[df.tmp==2, 'name'].apply(lambda x: unit_dict[x[-1].lower()[:-1]])
df.loc[df.tmp==1, 'unit_remains'] = 1
df.loc[df.tmp==2, 'unit_remains'] = 1

# Put name back together:
df.name = df.name.apply(concat_name, args=(unit_dict, pharma_units))

# Tidy up double spaces:
df.name = df.name.apply(remove_multiple_spaces)

# Drop the helper columns:
df.drop('tmp', axis=1, inplace=True)

print('Pharma products DONE.')
#_______________________________________________________________________________________________________________________
# Retailer packs 1
#_______________________________________________________________________________________________________________________
# Get a smaller subset of the data that are candidates for retailer packs to speedup regex:
units = [key for key, val in unit_dict.items() if val=='Und']
df['ContainsUnit'] = df.name.apply(lambda x: 1 if any(unit in x for unit in units) else 0)
units = [key for key, val in unit_dict.items() if (val!='Und') and (val!='mg')]
df['ContainsOtherUnit'] = df.name.apply(lambda x: 1 if any(unit in x for unit in units) else 0)
df.loc[(df.ContainsUnit==1)&(df.ContainsOtherUnit==1), 'tmp'] = 99
df.drop('ContainsUnit', axis=1, inplace=True)
df.drop('ContainsOtherUnit', axis=1, inplace=True)
# Case 4: quantity unit type quantity unit type that is mapped to 'Und':
myregex = list()
for key1, value1 in unit_dict.items():
    if (value1 != 'Und') and (value1 != 'mg'): # Don't touch pharmaproducts
        for key2, value2 in unit_dict.items():
            if value2 == 'Und':
                #myregex.append(r'{}\{}\{}'.format('(?i)(^|[ ])(', 'd+ ?' + value1 + ' ?x? ?', 'd+ ?' + key2 + 's?.? ?)($|\s)'))
                myregex.append(r'{}\{}\{}\{}'.format('(?i)(^|[ ])(', 'd*\.?', 'd+ ?' + value1 + ' ?x? ?', 'd+ ?' + key2 + 's?\.? ?)($|\s)')) # TODO Should be key1, check if we can catch most of the casesthis way!
               # myregex.append(r'{}\{}\{}'.format('(?i)(', ' d+ ?' + key1 + ' ?', 'd+ ?' + key2 + 's?.? ?)($|\s)'))
myregex = list(np.unique(myregex))
regex = '|'.join(myregex)
# Flag them:
mask = ((df.unit_remains==0)&(df.type!='Pack')&(df.tmp==99))
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 4
# Add presentation, quantity and unit_type:
df.loc[(df.tmp==4), 'presentation'] = df.loc[(df.tmp==4), 'name'].apply(presentation_pack, args=(regex, 4))
df.loc[(df.tmp==4), 'quantity'] = df.loc[(df.tmp==4), 'name'].apply(quantity_pack, args=(regex, 1))
df.loc[(df.tmp==4), 'unit_type'] = df.loc[(df.tmp==4), 'name'].apply(unit_pack, args=(regex, unit_dict, 4))
# Clean the name:
df.loc[(df.tmp == 4), 'name'] = df.loc[(df.tmp == 4), 'name'].apply(remove_unit, args=(regex,))
# Remove whitespaces:
df.loc[(df.tmp == 4), 'name'] = df.loc[(df.tmp == 4), 'name'].apply(remove_multiple_spaces)
# Make sure they don't get changed afterwards:
df.loc[(df.tmp==4), 'unit_remains'] = 1

# Case 1: quantity unit_type x quantity
myregex = list()
for key in unit_dict.keys():
    myregex.append(r'{}\{}\{}\{}'.format('(?i)(', 'd*\.?','d+ ?' + key + 's?\.? ?x ?', 'd+ ?)($|\s)'))
    #myregex.append(r'{}\{}\{}'.format('(?i)(', 'd+ ?' + key + 's?.? ?x ?', 'd+ ?)'))
regex = '|'.join(myregex)
# Flag them:
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 1
# Add presentation, quantity and unit_type:
df.loc[(df.tmp==1), 'presentation'] = df.loc[(df.tmp==1), 'name'].apply(presentation_pack, args=(regex, 1))
df.loc[(df.tmp==1), 'quantity'] = df.loc[(df.tmp==1), 'name'].apply(quantity_pack, args=(regex, 1))
df.loc[(df.tmp==1), 'unit_type'] = df.loc[(df.tmp==1), 'name'].apply(unit_pack, args=(regex, unit_dict))
# Clean the name:
df.loc[(df.tmp == 1), 'name'] = df.loc[(df.tmp == 1), 'name'].apply(remove_unit, args=(regex,))
# Remove whitespaces:
df.loc[(df.tmp == 1), 'name'] = df.loc[(df.tmp == 1), 'name'].apply(remove_multiple_spaces)
# Make sure they don't get changed afterwards:
df.loc[(df.tmp==1), 'unit_remains'] = 1

# Case 2: quantity x quantity unit type:
myregex = list()
for key in unit_dict.keys():
    myregex.append(r'{}\{}\{}'.format('(?i)(', 'd+ ?x ?', 'd+ ?' + key + 's?\.? ?)($|\s)'))
regex = '|'.join(myregex)
# Flag them:
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 2
# Add presentation, quantity and unit_type:
df.loc[(df.tmp==2), 'presentation'] = df.loc[(df.tmp==2), 'name'].apply(presentation_pack, args=(regex, 2))
df.loc[(df.tmp==2), 'quantity'] = df.loc[(df.tmp==2), 'name'].apply(quantity_pack, args=(regex, 2))
df.loc[(df.tmp==2), 'unit_type'] = df.loc[(df.tmp==2), 'name'].apply(unit_pack, args=(regex, unit_dict))
# Clean the name:
df.loc[(df.tmp == 2), 'name'] = df.loc[(df.tmp == 2), 'name'].apply(remove_unit, args=(regex,))
# Remove whitespaces:
df.loc[(df.tmp == 2), 'name'] = df.loc[(df.tmp == 2), 'name'].apply(remove_multiple_spaces)
# Make sure they don't get changed afterwards:
df.loc[(df.tmp==2), 'unit_remains'] = 1

# Case 3:  x quantity unit type where unit type is mapped to 'Und'
myregex = list()
for key, value in unit_dict.items():
    if value == 'Und':
        myregex.append(r'{}\{}\{}'.format('(?i)(', ' x ?', 'd+ ?' + key + 's?\.? ?)($|\s)'))
regex = '|'.join(myregex)
# Flag them:
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 3
# Add presentation, quantity and unit_type:
df.loc[(df.tmp==3), 'presentation'] = df.loc[(df.tmp==3), 'name'].apply(presentation_pack, args=(regex, 3))
df.loc[(df.tmp==3), 'quantity'] = 1
df.loc[(df.tmp==3), 'unit_type'] = 'Und'
# Clean the name:
df.loc[(df.tmp == 3), 'name'] = df.loc[(df.tmp == 3), 'name'].apply(remove_unit, args=(regex,))
# Remove whitespaces:
df.loc[(df.tmp == 3), 'name'] = df.loc[(df.tmp == 3), 'name'].apply(remove_multiple_spaces)
# Make sure they don't get changed afterwards:
df.loc[(df.tmp==3), 'unit_remains'] = 1

# Drop the helper columns:
df.drop('tmp', axis=1, inplace=True)

#_______________________________________________________________________________________________________________________
# Retailer packs.
#_______________________________________________________________________________________________________________________
# Flag anything that has a + in the title to avoid having its name changed by other manipulations.
# The name should just have units removed if they are there, quantity set to 1 and unit type as Und.
# Flag those entries:
df.loc[(df.name.str.contains(' \+ ')) & (df.unit_remains==0), 'tmp'] = 1
# Generate regex to find unit:
myregex = list()
for key, value in unit_dict.items():
    if value == 'Und':
        myregex.append(r'{}\{}'.format('(?i)(', 'd+ ?' + key + 's?)'))
regex = '|'.join(myregex)
# Flag the ones that contain the regex:
mask = (df.tmp==1)
df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 2
# Remove unit from name:
df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_unit, args=(regex,))
# Tidy up double spaces:
df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_multiple_spaces)
# Set quantity to 1:
df.loc[df.tmp==2, 'quantity'] = 1
# Set unit_type to 'Und':
df.loc[df.tmp==2, 'unit_type'] = 'Und'
# Flag to not change the name afterwards:
df.loc[df.tmp==2, 'unit_remains'] = 1
# Copy name into description:
df.loc[df.tmp==2, 'description'] = df.loc[df.tmp==2, 'name']
# Remove all units from name:
myregex = list()
for key, value in unit_dict.items():
    myregex.append(r'{}\{}\{}\{}'.format('(?i)(x? ?', 'd*', '\.?', 'd+ ?' + key + 's?)'))
    # myregex.append(r'{}\{}'.format('(?i)(x? ?', 'd+ ?' + key + 's?)'))
regex = '|'.join(myregex)
df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_unit, args=(regex,))
# Remove double spaces:
df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_multiple_spaces)
# Drop helper column:
df.drop('tmp', axis=1, inplace=True)

print('Retailer packs DONE.')
#_______________________________________________________________________________________________________________________
# Identify and clean packs
#_______________________________________________________________________________________________________________________
# Identify packs created by the retailer through the unit_type, which is 'Pack'.
# Here nothing should be changed in the name, just unit_type and quantity adjusted.
# For these products, their name remains the same.
df.loc[df.unit_type=='Pack', 'unit_remains'] = 1
# Set quantity to 1 and unit_type to 'Und:
df.loc[df.unit_type=='Pack', 'quantity'] = 1
df.loc[df.unit_type=='Pack', 'unit_type'] = 'Und'

#_______________________________________________________________________________________________________________________
# Packs created by Rappi with type 'Pack'
#_______________________________________________________________________________________________________________________
if run_packs == 'y':
    # Packs of cups, bottles or of the type 2 U.
    unit_regex = list()
    others_regex = list()
    for key, value in unit_dict.items():
        if value == 'Und' or value == 'Taza(s)':
            unit_regex.append(r'{}\{}\{}\{}'.format('(?i)(', 'd*', '\.', 'd+ ' + key + 's? )'))
            unit_regex.append(r'{}\{}'.format('(?i)(', 'd+ ' + key + 's? )'))
        else:
            others_regex.append(r'{}\{}\{}\{}'.format('(?i)(', 'd*', '\.', 'd+ ' + key + 's? )'))
            others_regex.append(r'{}\{}'.format('(?i)(', 'd+ ' + key + 's? )'))

    # Flag them:
    mask = (df.unit_remains==0)
    df.loc[mask & (df[mask].type=='Pack'), 'tmp'] = 1
    # Split unit and quantity:
    df.loc[(df.tmp==1), 'name'] = df.loc[(df.tmp==1), 'name'] .apply(split_unit_quantity, args=(unit_regex,))
    # Tidy up double spaces:
    df.loc[(df.tmp==1), 'name'] = df.loc[(df.tmp==1), 'name'].apply(remove_multiple_spaces)
    # Push the unit and quantity to the end of the name:
    df.loc[(df.tmp==1), 'name'] = df.loc[(df.tmp==1), 'name'].apply(unit_to_end, args=(unit_regex,unit_dict))
    # Tidy up double spaces:
    df.loc[(df.tmp==1), 'name'] = df.loc[(df.tmp==1), 'name'].apply(remove_multiple_spaces)
    # Split the name at spaces and commata:
    df.loc[(df.tmp==1), 'name'] = df.loc[(df.tmp==1), 'name'].apply(split_name, args=(unit_dict,))
    # Extract the quantity and put in new field for cups:
    cups_dict = {key:value for key, value in unit_dict.items() if (value=='Taza(s)')}
    df.loc[(df.tmp==1), 'quantity'] = df.loc[(df.tmp==1), 'name'].apply(lambda x: x[-2] if x[-1] in cups_dict.keys() else (1 if ((x[-1].replace('.','',1).isdigit() and x[-2].lower()=='x') or x[-1] in unit_dict.keys()) else np.NaN ))
    df.loc[(df.tmp==1), 'unit_type'] = df.loc[(df.tmp==1), 'name'].apply(lambda x: unit_dict[x[-1]] if x[-1] in cups_dict.keys() else ('Und' if ((x[-1].replace('.','',1).isdigit() and x[-2].lower()=='x') or x[-1] in unit_dict.keys()) else np.NaN))
    # Discount description for bottles:
    bottle_dict = {key:value for key, value in unit_dict.items() if (('botella' in key.lower()))}
    df.loc[(df.tmp==1), 'discount_description'] = df.loc[(df.tmp==1), 'name'].apply(lambda x: x[-2] if ((x[-1].lower() in bottle_dict.keys()) or (x[-1].lower()[:-1] in bottle_dict.keys())) else np.NaN )
    # Discount description for units:
    und_dict = {key:value for key, value in unit_dict.items() if (value=='Und')}
    df.loc[(df.tmp==1), 'discount_description'] = df.loc[(df.tmp==1), 'name'].apply(lambda x: x[-2] if ((x[-1].lower() in und_dict.keys()) or (x[-1].lower()[:-1] in und_dict.keys())) else np.NaN )
    # Put name back together:
    pack_dict = {key:value for key, value in unit_dict.items() if ('botella' in key.lower() or value=='Taza(s)' or value=='Und')}
    df.loc[(df.tmp==1), 'name'] = df.loc[(df.tmp==1), 'name'].apply(concat_name, args=(pack_dict, None))
    # Tidy up double spaces:
    df.name = df.name.apply(remove_multiple_spaces)

    # Packs of the type 'x 2':
    myregex = ['(?i)x ?\d+($|\s)', '(?i)\d+ ?x($|\s)']
    regex = '|'.join(myregex)
    # Identify the packs:
    mask = (df.tmp==1)
    df.loc[mask & df[mask].name.str.contains(regex, regex=True), 'tmp'] = 2
    # Change the discount_description:
    df.loc[df.tmp==2, 'discount_description'] = df.loc[df.tmp==2, 'name'].apply(get_discount_description, args=(regex, None))
    # Change the name:
    df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_unit, args=(regex, ))
    # Tidy up double spaces:
    df.name = df.name.apply(remove_multiple_spaces)
    # Drop helper column:
    df.drop('tmp', axis=1, inplace=True)

    # Packs that start with 'quantity':
    # Flag items with type 'Pack':
    df.loc[df.type=='Pack', 'tmp'] = 1
    # Find the ones where the name start with a quantity:
    myregex = ['(?i)(^\d+x\s)','(?i)(^\d+ ?u\s)']
    regex = '|'.join(myregex)
    df.loc[(df.tmp==1)&(df.name.str.contains(regex, regex=True)), 'tmp'] = 2
    # Remove the quantity from the name and put it into the discount description:
    df.loc[df.tmp==2, 'discount_description'] = df.loc[df.tmp==2, 'name'].apply(quantity_pack, args=(regex, 1))
    df.loc[df.tmp==2, 'name'] = df.loc[df.tmp==2, 'name'].apply(remove_unit, args = (regex, ))
    # Find the ones where the name start with a quantity:
    regex = r'(?i)(^\d+ (?!up))' # Manually exclude seven up :)
    df.loc[(df.tmp==1)&(df.name.str.contains(regex, regex=True)), 'tmp'] = 3
    # Remove the quantity from the name and put it into the discount description:
    df.loc[df.tmp==3, 'discount_description'] = df.loc[df.tmp==3, 'name'].apply(quantity_pack, args=(regex, 1))
    df.loc[df.tmp==3, 'name'] = df.loc[df.tmp==3, 'name'].apply(remove_unit, args = (regex, ))
    # Drop helper column:
    df.drop('tmp', axis=1, inplace=True)
    # The rest of the processing will be done in the general name processing at the end of the script.
    print('Packs DONE.')
#_______________________________________________________________________________________________________________________
# Remove unit and quantity from name
#_______________________________________________________________________________________________________________________
# The name field is cleaned as follows:
# 1.) Normalize the name, so that if a unit appears in one way or another, it is always quantity + space + unit, e.g. "Coca cola 1L bottle" ---> "Coca cola 1 L bottle".
# 2.) Push quantity and unit to the end of the name, e.g. "Coca cola 1 L bottle" ---> "Coca cola bottle 1 L".
# 3.) Remove quantity and unit from name and add instead to quantity and unit_type, e.g. name  =  'Coca cola bottle', unit_type =  L, quantity =  1.
# 4.) Cosmetics (lowercase/uppercase letters as specified) , e.g. name  =  'Coca Cola Bottle', unit_type =  L, quantity =  1.

# Now process the other ones:
# Define the regular expressions necessary to find the units and quantities, separated by space, in the name:
myregex = list()
for key, value in unit_dict.items():
    if (value != 'Und') and (value != 'mg'): # To exclude pharma products
        if value == 'm':
            myregex.append(r'{}\{}\{}\{}'.format('(?i)(^|\s)(?=x)(x ?', 'd*', '.', 'd+ ?' + key + ')($|\s)'))
            myregex.append(r'{}\{}'.format('(?i)(^|\s)(?=x)(x ?', 'd+ ?' + key + ')($|\s)'))
            myregex.append(r'{}\{}\{}\{}'.format('(?i)(^|\s)(', 'd*', '.', 'd+ ?' + key + ')($|\s)'))
            myregex.append(r'{}\{}'.format('(?i)(^|\s)(', 'd+ ?' + key + ')($|\s)'))#^gs?.?
        else:
            myregex.append(r'{}\{}\{}\{}'.format('(?i)(^|\s)(?=x)(x ?', 'd*', '.', 'd+ ?' + key + 's? ?\.?)($|\s)'))
            myregex.append(r'{}\{}'.format('(?i)(^|\s)(?=x)(x ?', 'd+ ?' + key + 's? ?\.?)($|\s)'))
            myregex.append(r'{}\{}\{}\{}'.format('(?i)(^|\s)(', 'd*', '.', 'd+ ?' + key + 's? ?\.?)($|\s)'))
            myregex.append(r'{}\{}'.format('(?i)(^|\s)(', 'd+ ?' + key + 's? ?\.?)($|\s)'))
regex = '|'.join(myregex)

# Split unit and quantity:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(split_unit_quantity, args=(myregex, )) # needs to be done again in case name contains unit und AND another one.

# Tidy up double spaces:
df.name = df.name.apply(remove_multiple_spaces)

# Push the unit and quantity to the end of the name:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(unit_to_end, args=(myregex, unit_dict))

# Remove '.' from the end of the string:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(lambda x: x[:-1] if x[-1]=='.' else x)

# Tidy up double spaces:
df.name = df.name.apply(remove_multiple_spaces)

# Split the name at spaces and commata:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(split_name, args=(unit_dict,))

# Extract the quantity and put in new field:
mask = (df.unit_remains == 0)
df.loc[mask, 'tmp'] = df.loc[mask, 'name'].apply(lambda x: 1 if (len(x)>1) and ((x[-1].lower() in unit_dict.keys()) or (x[-1].lower()[:-1] in unit_dict.keys())) and (x[-1].lower() != 'mg') and (x[-2].replace('.','',1).isdigit()) else 0)
df.loc[df.tmp==1, 'quantity'] = df.loc[df.tmp==1, 'name'].apply(lambda x: x[-2])
df.loc[df.tmp==1, 'unit_type'] = df.loc[df.tmp==1, 'name'].apply(lambda x: unit_dict[x[-1].lower()] if x[-1].lower() in unit_dict.keys() else unit_dict[x[-1].lower()[:-1]])

# Put name back together:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(concat_name, args=(unit_dict, None))

# Tidy up double spaces:
df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(remove_multiple_spaces)

# Drop helper column:
df.drop('tmp', axis=1, inplace=True)

print('Remaining products DONE.')
#_______________________________________________________________________________________________________________________
# Cosmetics:
#_______________________________________________________________________________________________________________________
# -- Remove '1U', '1x' from name if in the end (leftovers from cleaning process)
# -- Remove ',', '.', '()', ';' if in the end.

# Split unit and quantity:
# df.loc[df.unit_remains==0, 'name'] = df.loc[df.unit_remains==0, 'name'].apply(split_unit_quantity, args=(myregex, ))
#
# # Tidy up double spaces:
# df.name = df.name.apply(remove_multiple_spaces)

# Split the name at spaces and commata:
df.name = df.name.apply(split_name, args=(unit_dict,))

# Put name back together:
df.name = df.name.apply(remove_1s, args=(unit_dict, ))

# Tidy up double spaces:
df.name = df.name.apply(remove_multiple_spaces)

# Remove flag columns:
df.drop('unit_remains', axis=1, inplace=True)

print('Cosmetics DONE.')
#_______________________________________________________________________________________________________________________
# Remove commata and semicolons
#_______________________________________________________________________________________________________________________
df.name = df.name.str.replace(';', '')
df.name = df.name.str.replace(',', '')
df.description = df.description.str.replace(';', '')
df.description = df.description.str.replace(',', '')
#_______________________________________________________________________________________________________________________
# Fill the unit_type:
#_______________________________________________________________________________________________________________________
# Change the unit_type back to the original one, if it is mapped:
if any(df.unit_type.isna()):
    df.loc[df.unit_type.isna(), 'unit_type'] = df_orig.loc[df.unit_type.isna(), 'unit_type'].apply(lambda x: unit_dict[x.lower()] if (str(x).lower() in unit_dict.keys()) else None)
df.loc[df.unit_type=='Unchanged', 'Changed_unit_type'] = 1
df.loc[df.unit_type=='Unchanged', 'unit_type'] = None
df.unit_type = df.unit_type.str.strip()

# For Colombia special cleaning must be done:
#mL = Mililitro a
#Und = Unidad a
#g = Gramo a
if 'country' in df.columns:
    df.loc[df.country=='CO', 'unit_type'] = df.loc[df.country=='CO', 'unit_type'].replace({'mL': 'Mililitro a', 'Und': 'Unidad a', 'g': 'Gramo a'})
    # Convert kg and Liters:
    df.loc[(df.country=='CO')&(df.unit_type=='L'), 'quantity'] = np.round(df.loc[(df.country=='CO')&(df.unit_type=='L'), 'quantity'].astype(float) * 1000, 0)
    df.loc[(df.country=='CO')&(df.unit_type=='L'), 'unit_type'] = 'Mililitro a'
    df.loc[(df.country=='CO')&(df.unit_type=='Kg'), 'quantity'] = np.round(df.loc[(df.country=='CO')&(df.unit_type=='Kg'), 'quantity'].astype(float) * 1000, 0)
    df.loc[(df.country=='CO')&(df.unit_type=='Kg'), 'unit_type'] = 'Gramo a'
#_______________________________________________________________________________________________________________________
# Fill the quantity:
#_______________________________________________________________________________________________________________________
# Change the quantity back to the original one, if it is empty:
if 'quantity' in df_orig.columns:
    if any(df.quantity.isna()):
        df.loc[df.quantity.isna(), 'quantity'] = df_orig.loc[df.quantity.isna(), 'quantity']
# Round it:
df.loc[~df.quantity.isna(), 'quantity'] = df.loc[~df.quantity.isna(), 'quantity'].apply(lambda x: np.round(float(x), 2) if str(x)[::-1].find('.') > 2 else x)
print('Unit type and quantity processing DONE.')

#_______________________________________________________________________________________________________________________
# Tidy up the presentation:
#_______________________________________________________________________________________________________________________
df.presentation = df.presentation.astype(str)
df.loc[~df.presentation.isna(), 'presentation'] = df.loc[~df.presentation.isna(), 'presentation'].apply(tidy_presentation, args=(unit_dict_cleanup,))

# Change the presentation for packs:
mask = (df.type=='Pack')
df.loc[mask & (~df[mask].discount_description.isna()) & (df[mask].unit_type!='Und'), 'presentation'] = df.loc[mask & (~df[mask].discount_description.isna()) & (df[mask].unit_type!='Und'), 'discount_description'].astype(str) + ' x ' + df.loc[mask & (~df[mask].discount_description.isna()) & (df[mask].unit_type!='Und'), 'quantity'].astype(str) + ' ' + df.loc[mask & (~df[mask].discount_description.isna()) & (df[mask].unit_type!='Und'), 'unit_type']
df.loc[mask & (df[mask].discount_description.isna()), 'presentation'] = df.loc[mask & (df[mask].discount_description.isna()), 'quantity'].astype(str) + ' ' + df.loc[mask & (df[mask].discount_description.isna()), 'unit_type']
df.loc[mask & (df[mask].unit_type=='Und'), 'presentation'] = df.loc[mask & (df[mask].unit_type=='Und'), 'quantity'].astype(str) + ' ' + df.loc[mask & (df[mask].unit_type=='Und'), 'unit_type']


#_______________________________________________________________________________________________________________________
# Tidy up the description:
#_______________________________________________________________________________________________________________________
if language == 'Spanish':
    df.loc[~df.description.isna(), 'description'] = df.loc[~df.description.isna(), 'description'].str.replace('- Precios con IVA incluido', '')
    df.loc[~df.description.isna(), 'description'] = df.loc[~df.description.isna(), 'description'].str.replace('- Precio con IVA incluido', '')
#_______________________________________________________________________________________________________________________
# Remove 'Combos' from type:
#_______________________________________________________________________________________________________________________
df.loc[~df.type.isna(), 'type'] = df.loc[~df.type.isna(), 'type'].str.replace('Combos', '')

#_______________________________________________________________________________________________________________________
# Capitalize the name
#_______________________________________________________________________________________________________________________
df.name = df.name.apply(capitalize_name)
#_______________________________________________________________________________________________________________________
# Create sample and write to csv
#_______________________________________________________________________________________________________________________
# Create dataframe with ID and all fields that may have changed:
df_sample = df_orig.copy()
cols = ['id', 'name', 'description', 'type', 'quantity', 'unit_type', 'discount_type', 'discount_description', 'presentation']
dropcols = [col for col in df_sample.columns if col not in cols]
df_sample.drop(dropcols, axis=1, inplace=True)
for col in cols:
    df_sample[col + '_new'] = df[col]
# Write to csv:
today = dt.datetime.today().strftime('%Y%m%d')
if 'idfile' in locals():
    df_sample.to_csv(datafile.split('.')[0] + '_' + today + '_' + idfile.split('.')[0] + '_clean_testing_' + language + '.csv', index=False)
    df.to_csv(datafile.split('.')[0] + '_' + today + '_' + idfile.split('.')[0] +  '_clean_' + language + '.csv', index=False)
else:
    df_sample.to_csv(datafile.split('.')[0] + '_' + today + '_' + '_clean_testing_' + language + '.csv', index=False)
    df.to_csv(datafile.split('.')[0] + '_' + today + '_' + '_clean_' + language + '.csv', index=False)
tt = time.time()-tt
print('Took ' + str(tt/60) + ' to run.')

# Find strange unit_types:
units = [unit for unit in df.unit_type.unique() if unit not in unit_dict.values()]
if None in units:
    units.remove(None)
if '' in units:
    units.remove('')
# Add types for Colombia:
units_CO = units[:]
if 'Mililitro a' in units_CO:
    units_CO.remove('Mililitro a')
if 'Unidad a' in units_CO:
    units_CO.remove('Unidad a')
if 'Gramo a' in units_CO:
    units_CO.remove('Gramo a')

df['country_id'] = df['country'] + '_' + df['id'].astype(str)
df_tmp = df[((df.country!='CO') & (df.unit_type.isin(units)))|((df.country=='CO') & (df.unit_type.isin(units_CO)))]
df = df[~df.country_id.isin(df_tmp.country_id.values)]

df.drop('country_id', axis=1, inplace=True)
df_tmp.drop('country_id', axis=1, inplace=True)

df_tmp.to_csv(datafile.split('.')[0] + '_' + today + '_' + language + '_unit_types.csv', index = False)
# Split into several files for upload:
for i in range(6):
    df[i*100000:(i+1)*100000].to_csv(datafile.split('.')[0] + '_' + today + '_clean_' + language + '_' + str(i) + '.csv', index=False)



