import pandas as pd
import numpy as np
import re
#_______________________________________________________________________________________________________________________
# Functions
#_______________________________________________________________________________________________________________________
# Create a dictionary to rename the unit_types.
def add_units_to_dict(dictionary, units, key):
    """
    Adds items in units to dictionary, giving them the specified key.
    Input:
    -- dictionary: python dictionary
    -- units: list of "units" that should be mapped to a certain key (e.g. ['unid', 'un'])
    -- key: real units the misspelled ones should be replaced with (e.g. 'Und')
    Output:
    -- dictionary: python dictionary containing the name replacements for the unit_type
    """
    for item in units:
        dictionary[item] = key
    return dictionary

# Make sure words are capitalized, connectors are not:
def capitalize_name(x):
    """
    Function that capitalizes every word except for connectors.
    Input:
    -- x: string to be processed
    Output:
    -- x: processed string
    """
    # Define the connectors:
    connectors_low=['y', 'de', 'a', 'en', 'o', 'u', 'e']
    connectors_cap=['Y', 'De', 'A', 'En', 'O', 'U', 'E']
    # Process the string word by word:
    words = []
    for word in x.split():
        if word not in connectors_low:
            word = word.capitalize()
        if word in connectors_cap:
            word = word.lower()
        words.append(word)
    return " ".join(words)

# Remove double spaces:
def remove_multiple_spaces(x):
    x = x.strip()
    x = x.replace('( ', '(')
    return ' '.join(x.split())

def unit_to_end(x, myregex, unit_dict):
    for regex in myregex:
        result = re.search(regex, x)
        if result is not None:
            if any(item in result[0] for item in [key for key, value in unit_dict.items() if value=='Taza(s)']):
                x = x.replace(result[0], 'Taza')
            else:
                x = x.replace(result[0], ' ')
            x = x + ' ' + result[0].replace(' .', '')
            break
    return x

# Split unit and quantity:
def split_unit_quantity(x, myregex):
    for regex in myregex:
        result = re.search(regex, x)
        if result is not None:
            if re.search('(\d+\.\d+)', result[0]) is not None:
                x = re.sub(regex, ' ' + re.sub('(\d+\.\d+)', r' \1 ', result[0]) + ' ', x).strip()
            else:
                x = re.sub(regex, ' ' + re.sub('(\d+)', r' \1 ', result[0]) + ' ', x).strip()
            #x = re.sub('(\d+(\.\d+)?)', r' \1 ', x).strip()
            break # For performance reasons
    return x

def set_quantity_square(x, myregex):
    result = re.search(myregex, x)
    quantity = re.findall('\d+', result[0])[0]
    return quantity

def remove_unit(x, myregex):
    return re.sub(myregex, '', x)

def tidy_presentation(x, unit_dict):
    out = x.strip()
    if out.lower()+'s' in unit_dict.keys():
        out = unit_dict[out.lower()+'s']
    elif out.lower() in unit_dict.keys():
        out = unit_dict[out.lower()]
    else:
        for key, value in unit_dict.items():
            if '(' in key:
                key = key.replace('(', '\(')
            if ')' in key:
                key = key.replace(')', '\)')
            regex = '(?i)( ?\d+ ?' + key + 's?\.?)($|\s)'
            result = re.search(regex, x.lower())
            if result is not None:
                out = re.sub(r'(' + key + 's?\.?)', value, x.lower()).strip()
                break
    return out

def get_discount_description(x, myregex, promo_type):
    result = re.search(myregex, x)
    if result is not None:
        allnumbers = re.findall('(\d+)', result[0])
        if promo_type == 'deal':
            if len(allnumbers) > 1: # lleve x pague y
                return allnumbers[1] + '|' + allnumbers[0]
            else:
                return allnumbers[0]
        elif promo_type == 'Deal_inv':
            if len(allnumbers) > 1: # pague y lleve x
                return allnumbers[0] + '|' + allnumbers[1]
            else:
                return allnumbers[0]
        elif promo_type == 'Deal_inv_quantity':
            if len(allnumbers) > 2: # pague y lleve z unit type x y
                return allnumbers[0] + '|' + allnumbers[2]
            else:
                return allnumbers[0]
        elif (promo_type == 'percentage') or (promo_type=='markdown'):
            if len(allnumbers) > 1: # items of the type x% en y unidad
                return allnumbers[1] + '|' + allnumbers[0] + '%'
            else:
                return allnumbers[0] + '%'
        else:
            return allnumbers[0] # for packs where only one unit is there like xU.

def get_quantity_discount(x, regex):
    result = re.search(regex, x)
    if result is not None:
        allnumbers = re.findall('(\d+)', result[0])
    return allnumbers[1]

def get_unit_type_discount(x, unit_dict, regex):
    result = re.search(regex, x)[0]
    myregex = list()
    for key in unit_dict.keys():
        myregex.append('(?i)( ?\d+ ?' + key + 's? ?(?![A-Za-z]))') #[A-Za-z]
    regex = '|'.join(myregex)
    result = re.search(regex, result)
    if result is not None:
        unit_type = re.sub('\d+', '', result[0]).strip()
        if unit_type.lower() in unit_dict.keys():
            unit_type = unit_dict[unit_type.lower()]
        elif unit_type.lower()[:-1] in unit_dict.keys():
            unit_type = unit_dict[unit_type.lower()[:-1]]
        return unit_type
    else:
        print('UNIT TYPE NOT KNOWN ' + x)
        return 'Unchanged'

def find_packs(x, myregex):#, others_combined):
    x = "".join(x.split())
    for regex in myregex:
        result = re.search(regex, x)
        if (result is not None):#and (re.search(others_combined, x) is not None): # For now count everything with more than one unit as a pack
            return 1

def split_name(x, unit_dict):
    x = x.replace(',', ' ').split(' ')
    if x[-1] == 'de':
        to_add = x[-2] + ' ' + x[-1]
        x = x[:-2]
        x.append(to_add)
    return x

def concat_name(listofstrings, unit_dict, pharma_units):
    if (isinstance(listofstrings, list)):
        if ((listofstrings[-1].lower() in unit_dict.keys())) and ((unit_dict[listofstrings[-1].lower()] != 'mg') ):
            if pharma_units is not None:
               if (listofstrings[-1].lower() not in unit_dict.keys()) or ((listofstrings[-1].lower() in unit_dict.keys()) and unit_dict[listofstrings[-1].lower()] not in pharma_units):
                   return ' '.join(listofstrings)
               else:
                   if (len(listofstrings) > 1) and (listofstrings[-2].replace('.', '', 1).isdigit()):
                       if (len(listofstrings) > 3) and (listofstrings[-3].strip().lower() == 'x') and not (listofstrings[-4].replace('.', '', 1).isdigit()):
                           return ' '.join(listofstrings[:-3])
                       else:
                           return ' '.join(listofstrings[:-2])
                   else:
                       return ' '.join(listofstrings)
            else:
                if (listofstrings[-1].lower() not in unit_dict.keys()): # Exception for pharma products
                    return ' '.join(listofstrings)
                else:
                    if (len(listofstrings) > 1) and (listofstrings[-2].replace('.','',1).isdigit()): # ends in number unit
                        if (len(listofstrings) > 2) and (listofstrings[-3].strip().lower() == 'x') and not (listofstrings[-4].replace('.', '', 1).isdigit()): # if the number of units should stay like 4x350g
                            return ' '.join(listofstrings[:-3])
                        else:
                            return ' '.join(listofstrings[:-2])
                    else:
                        return ' '.join(listofstrings)
        elif (listofstrings[-1].lower()[:-1] in unit_dict.keys()) and (unit_dict[listofstrings[-1].lower()[:-1]] != 'mg'):
            if pharma_units is not None:
               if (listofstrings[-1].lower()[:-1] not in unit_dict.keys()) or ((listofstrings[-1].lower()[:-1] in unit_dict.keys()) and unit_dict[listofstrings[-1].lower()[:-1]] not in pharma_units):
                   return ' '.join(listofstrings)
               else:
                   if (len(listofstrings) > 1) and (listofstrings[-2].replace('.', '', 1).isdigit()):
                       if (len(listofstrings) > 3) and (listofstrings[-3].strip().lower() == 'x') and not (listofstrings[-4].replace('.', '', 1).isdigit()):
                           return ' '.join(listofstrings[:-3])
                       else:
                           return ' '.join(listofstrings[:-2])
                   else:
                       return ' '.join(listofstrings)
            else:
                if (listofstrings[-1].lower()[:-1] not in unit_dict.keys()): # Exception for pharma products
                    return ' '.join(listofstrings)
                else:
                    if (len(listofstrings) > 1) and (listofstrings[-2].replace('.','',1).isdigit()): # ends in number unit
                        if (len(listofstrings) > 2) and (listofstrings[-3].strip().lower() == 'x') and not (listofstrings[-4].replace('.', '', 1).isdigit()): # if the number of units should stay like 4x350g
                            return ' '.join(listofstrings[:-3])
                        else:
                            return ' '.join(listofstrings[:-2])
                    else:
                        return ' '.join(listofstrings)
        else:
            return ' '.join(listofstrings)
    else:
        return listofstrings

def remove_1s(x, unit_dict):
    if len(x) < 2:
        return ''.join(x)
    if (x[-1].lower() in [key for key, value in unit_dict.items() if value=='Und']) and (x[-2] == '1'):
        x = ' '.join(x[:-2])
    # Type 1 x
    elif (x[-1] == '1') and (x[-2].lower=='x'):
        x = ' '.join(x[:-2])
    # Type ',', '.', '()':
    elif x[-1].lower() in (',', '.', '()', 'com', 'con', 'de'):
        x = ' '.join(x[:-1])
    else:
        x = ' '.join(x)
    return x

def quantity_cu(x, regex, case=1):
    result = re.search(regex, x)[0]
    allnumbers = re.findall('\d+', result)
    # Case 1: Multiply
    if case == 1:
        x = int(allnumbers[0]) * int(allnumbers[1])
    # Case 2: Give second number
    elif case == 2:
        x = allnumbers[1]
    # Case 3: Give first (and only) number
    elif case == 3:
        x = allnumbers[0]
    return x

def check_promos(x, regex, mynumber):
    result = re.search(regex, x)[0]
    allnumbers = re.findall('\d+', result)
    if int(allnumbers[0]) > int(allnumbers[1]):
        return mynumber
    else:
        return 0

def unit_cu(x, regex, unit_dict, case=1):
    if case==1:
        result = re.search(regex, x)[0]
        unit = ''
        for key, value in unit_dict.items():
            if (value != 'Und') and ((' ' + key + ' ') in result.lower()): # Avoid that for example 'l' is found in botella and unit type set to that.
                unit = value
                break
    else:
        result = re.search(regex, x)[0]
        result = result.lower().split('con')[-1]
        for key, value in unit_dict.items():
            if (value != 'Und') and ((' ' + key + ' ') in result.lower()): # Avoid that for example 'l' is found in botella and unit type set to that.
                unit = value
                break
    return unit

def presentation_cu(x, regex, case=1, types=None):
    # Case one, remove the first letter (X) and return the rest.
    if case==1:
        x = re.search(regex, x)[0][1:]
        x = x.lower().replace('c/u', '')
    # Case 2: remove type and first letter (X), return the rest.
    if case==2:
        result = re.search(regex, x)[0]
        for mytype in types:
            if mytype in result.lower():
                x = result.lower().replace(mytype, '').strip()
                break
        x = x[1:]
        x = x.replace('c/u', '')
    # Case 3
    if case==3:
        result = re.search(regex, x)[0]
        allnumbers = re.findall('\d+', result)
        start = result.find(allnumbers[0]) + len(allnumbers[0])
        end = result.find(allnumbers[1], start)
        x = result.replace(result[start:end], ' X ')
        x = x.lower().replace('c/u', '').capitalize()
    if case==88:
        result = re.search(regex, x)[0]
        allnumbers = re.findall('\d+', result)
        start = result.find(allnumbers[0]) + len(allnumbers[0])
        end = result.find(allnumbers[1], start)
        x = result.replace(result[start:end], ' X ')
        x = x.lower().replace('c/u', '')
        x = x.lower().replace('con', '').capitalize()
    if case==33:
        result = re.search(regex, x)[0]
        allnumbers = re.findall('\d+', result)
        start = result.find(allnumbers[0])
        end = result.find(allnumbers[1], start) + len(allnumbers[1])
        x = result[start:end]
        x = x.lower().replace('c/u', ' x ')
    return x

def presentation_pack(x, regex, case = 1):
    result = re.search(regex, x)[0]
    if result is not None:
        if case==1: #300ml X 5
            if 'x' in result:
                tmp = result.split('x')
            else:
                tmp = result.split('X')
            out = tmp[1] + ' X ' + tmp[0]
        elif case==2: # 5x330ml
            out = result
        elif case==3: # x 10 und
            quantity = re.search('\d+', result)[0]
            out = quantity + ' Und'
        elif case==4: # x 10 und
            quantities = re.findall('\d+', result)
            out = quantities[1] + ' Und'
    return out

def quantity_pack(x, regex, case=1):
    result = re.search(regex, x)[0]
    allnumbers = re.findall('\d+', result)
    if result is not None:
        if case==1:
            out = allnumbers[0]
        elif case==2:
            out = allnumbers[1]
    return out

def unit_pack(x, regex, unit_dict, case=1):
    result = re.search(regex, x)[0]
    out = re.sub('\d+', ' ', result)
    if case==4:
        for key, value in unit_dict.items():
            if (value=='Und'):
                out = out.lower().replace(' ' + key + 's', '')
                out = out.lower().replace(' ' + key, '')
    out = out.strip()
    out = out.replace('x', '')
    out = out.replace('X', '')
    out = out.replace('.', '')
    out = out.strip()
    if out.lower() in unit_dict.keys():
        out = unit_dict[out.lower()]
    elif out.lower()[:-1] in unit_dict.keys():
        out = unit_dict[out.lower()[:-1]]
    return out

