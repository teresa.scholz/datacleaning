import pandas as pd
import numpy as np

pd.set_option('display.width', 500)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 70)
#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________
# Define datapath and filename:
datafolder_clean = '/home/koenigin/Desktop/Rappi_Data_Cleaning/Cleaning/'
#datafile_clean = 'ParentProducts_2020-7-5_2110_clean_spanish_20200713.csv'
datafile_clean = 'ParentProducts_2020-7-5_2110_clean_Portuguese_20200714.csv' #'ParentProducts_2020-7-5_2110_clean_Spanish_20200714.csv'
datafolder = '/home/koenigin/Desktop/Rappi_Data_Cleaning/Data/'
datafile = 'ParentProducts_2020-7-5_2110.csv'
language = 'Portuguese'

# Load the data:
df = pd.read_csv(datafolder_clean + datafile_clean)
# Original data:
# Load the data:
df_orig = pd.read_csv(datafolder + datafile)
# Change header to lowercase:
df_orig.columns = [col.lower() for col in df_orig.columns]
# Remove items without a name:
df_orig = df_orig[~df_orig.name.isna()]
df_orig = df_orig[df_orig.country=='br']
df_orig = df_orig[~df_orig.name.isna()]
# Find names that have line formatting in the name and remove it:
mystrs = ['\t', '\r', '\n']
for ss in mystrs:
    for col in ['name', 'description', 'type']:
        df_orig[col] = df_orig[col].str.replace(ss, '')

#_______________________________________________________________________________________________________________________
# Create test files:
#_______________________________________________________________________________________________________________________
for jj in range(5):
    ids_test = list()
    # Rappicombos/combos:
    tmp = df[df.name.str.contains('Combo')].id.values
    if len(tmp) > 10:
        counter = 0
        while counter < 10:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    tmp = df[df.name.str.contains('Rappicombo')].id.values
    if len(tmp) > 10:
        counter = 0
        while counter < 10:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Packs:
    tmp = df[df.type=='Pack'].id.values
    if len(tmp) > 10:
        counter = 0
        while counter < 10:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Promos:
    # Deals:
    tmp = df[df.discount_type=='Deal'].id.values
    if len(tmp) > 10:
        counter = 0
        while counter < 10:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Percentage:
    tmp = df[df.discount_type=='Percentage'].id.values
    if len(tmp) > 10:
        counter = 0
        while counter < 10:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Markdown:
    tmp = df[df.discount_type=='Markdown'].id.values
    if len(tmp) > 10:
        counter = 0
        while counter < 10:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Pharma products:
    tmp = df[df.name.str.contains('%')].id.values
    if len(tmp) > 5:
        counter = 0
        while counter < 5:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    tmp = df[df.name.str.lower().str.contains('mg')].id.values
    if len(tmp) > 5:
        counter = 0
        while counter < 5:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Retailer packs:
    tmp = df[df.name.str.contains('\+')].id.values
    if len(tmp) > 5:
        counter = 0
        while counter < 5:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Add 10 other random values:
    tmp = df[~df.id.isin(ids_test)].id.values
    if len(tmp) > 20:
        counter = 0
        while counter < 20:
            item = np.random.choice(tmp, 1, replace=False)
            if item not in ids_test:
                ids_test.append(item)
                counter += 1
    else:
        for item in tmp:
            if item not in ids_test:
                ids_test.append(item)
    # Drop into file:
    df_sample = df_orig[df_orig.id.isin(ids_test)].copy()
    df_tmp = df[df.id.isin(ids_test)].copy()
    df_join = pd.merge(df_tmp, df_sample, on=['id', 'country'], suffixes = ['_new', ''])
    cols = ['country', 'id', 'name', 'name_new', 'description_new', 'quantity_new', 'unit_type_new', 'type_new', 'discount_type_new', 'discount_description_new', 'presentation_new', 'description', 'quantity', 'unit_type', 'type', 'discount_type', 'discount_description', 'presentation']
    df_join = df_join[cols]
    df_join.to_csv(datafile + '_test_' + language + '_' + str(jj) + '.csv', index=False)

