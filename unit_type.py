import csv
from Cleaning_functions import add_units_to_dict

# Define the dictionary to clean the unit_type:
unit_dict = dict()

# Pills (Tabletas):
key = 'Pastilla(s)'
units = ['tabletas', 'tableta(s)', 'tabs', 'tab']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Pills (cápsulas):
key = 'Cápsula(s)'
units = ['cápsulas', 'capsulas', 'capsula', 'caps', 'cap']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Pills (comprimidos):
key = 'Comprimido(s)'
units = ['comprimidos', 'comprimido', 'comp', 'com']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Coated pills:
key = 'Gragea(s)'
units = ['gragea(s)', 'grageas', 'gragea', 'gg']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Cups:
key = 'Taza(s)'
units = ['taza(s)', 'tazas', 'taza']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Ampoules:
key = 'Ampolleta(s)'
units = ['ampolletas', 'ampolleta(s)', 'ampolleta', 'ampollas', 'amopolla(s)', 'ampolla']
unit_dict = add_units_to_dict(unit_dict, units, key)

## Units:
key = 'Und'
units = ['unidad(es)', 'unidade(s)', 'botellas de', 'botellas', 'unidades', 'unidad a', 'unidad', 'unid',
         'und', 'uni', 'un', 'u']
unit_dict = add_units_to_dict(unit_dict, units, key)

## Millilitres:
key = 'mL'
units = ['mililitro', 'ml']
unit_dict = add_units_to_dict(unit_dict, units, key)

## Litres:
key = 'L'
units = ['litro', 'lt', 'l']
unit_dict = add_units_to_dict(unit_dict, units, key)

## Grams:
key = 'g'
units = ['gramo(s)', 'gramos', 'gramo', 'grs', 'grm', 'gr', 'gs', 'g']
unit_dict = add_units_to_dict(unit_dict, units, key)

## Kilograms:
key = 'Kg'
units = ['kilogramos', 'kilogramo', 'kg']
unit_dict = add_units_to_dict(unit_dict, units, key)

## Miligrams:
key = 'mg'
units = ['miligramos', 'miligramo(s)', 'miligramo', 'mg']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Millimeters:
key = 'mm'
units = ['mm']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Centimeters:
key = 'cm'
units = ['cm', 'cms']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Cubic centimeters:
key = 'cc'
units = ['cc', 'ccs']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Meters:
key = 'm'
units = ['mts', 'mt', 'm']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Pounds:
key = 'Lb'
units = ['lb']
unit_dict = add_units_to_dict(unit_dict, units, key)

# Write to csv file:
with open('unit_dict.csv', 'w') as csv_file:
    writer = csv.writer(csv_file)
    for key, value in unit_dict.items():
       writer.writerow([key, value])

# Types for the 'c/u' cleaning:
types = ['caja', 'bolsa', 'vaso', 'frasco', 'sachet', 'botella', 'unidade', 'rollo', 'sobre', 'tubo', 'unid', 'barra', 'und', 'un', 'lata', 'pza', 'cj', 'gg', 'u']
# Write to csv file:
with open('types.csv', 'w') as csv_file:
    writer = csv.writer(csv_file)
    for value in types:
       writer.writerow([value])